-- SUMMARY --

Fast and easy integration of daily sudoku puzzles into your blogs. 

Major features in this version include: 

* The sudoku puzzles can be inserted everywhere: in your pages.
* Seven different levels from simple to diabolic are available 
* Use pencilmarks by pressing ALT and Number key
* they are daily updated and can be used as daily sudoku 
* You can change appearance, size and visible features 
* printable sudokus for offline esolving are available too


-- REQUIREMENTS --

* Client machine should be able to access http://mypuzzle.org

-- Installation --

1. Upload 'Sudoku Mypuzzle' folder to the '/sites/all/modules/' directory.
2. Activate the plugin through the 'Modules' menu in administration/games.
3. Configure Blocks to use the MyPuzzle Sudoku Module under Structure/Block
4. Enjoy the Pluging on pages using that block

-- CONFIGURATION -- 

Settings available for:
* Show history - Display the last 10 days daily sudoku for each level
* Show Levels - Display the choice to change between 7 levels of difficulty
* Show Print - Show a print link/button to print the current sudoku
* Size - The option to chose between to choices (smaller-default and large)
* Show Timer - Option to show a timer measure solving-time

-- CONTACT --

Current maintainers:

* Thomas Seidel - http://drupal.org/user/2260300
